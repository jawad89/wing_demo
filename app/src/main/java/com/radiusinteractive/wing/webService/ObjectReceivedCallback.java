package com.radiusinteractive.wing.webService;

/***
 * This interface provides the presenter the object received from as a response from a network call
 */
public interface ObjectReceivedCallback <T> {
    void onSuccessListener(T object, String msg);
    void onFailureListener(Throwable t);
}
