package com.radiusinteractive.wing.webService;


public class ApiResponse<T> {
    private T response;
    private Throwable error;

    public ApiResponse(T response) {
        this.response = response;
        this.error = null;
    }

    public ApiResponse(Throwable error) {
        this.error = error;
        this.response = null;
    }

    public T getResponse() {
        return response;
    }

    public Throwable getError() {
        return error;
    }
}
