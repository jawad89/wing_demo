package com.radiusinteractive.wing.util;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.view.View;

import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class Utils {

    public static boolean networkAvailable(Context context)
    {
        ConnectivityManager cc = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cc != null) {
            NetworkInfo[] info = cc.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
                }
            }
        }
        return false;
    }

    public static String formatTime(int timeInMillis){
        if (TimeUnit.MILLISECONDS.toHours(timeInMillis)>0)
            return String.format(Locale.ENGLISH,"%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(timeInMillis),
                    TimeUnit.MILLISECONDS.toMinutes(timeInMillis) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.MILLISECONDS.toSeconds(timeInMillis) % TimeUnit.MINUTES.toSeconds(1));
        else
            return String.format(Locale.ENGLISH,"%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(timeInMillis) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.MILLISECONDS.toSeconds(timeInMillis) % TimeUnit.MINUTES.toSeconds(1));
    }

    public static int dp2pxls(Context context, int dp)
    {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int)(dp*(metrics.densityDpi/160f));
    }

    public static int dp2pxls(Context context, float dp)
    {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int)(dp*(metrics.densityDpi/160f));
    }

    public static float pxToDp(Context context, float px)
    {
        final float scale = context.getResources().getDisplayMetrics().density;
        return  (px - 0.5f)/scale;
    }

    public static float CLAMP(float min, float max, float value)
    {
        return Math.max(Math.min(value, max), min);
    }

    public static int CLAMP(int min, int max, int value)
    {
        return Math.max(Math.min(value, max), min);
    }

    public static void showSnackbarMessage(final String message,View rootView){
        Snackbar.make(rootView,message, Snackbar.LENGTH_LONG).show();
    }
}
