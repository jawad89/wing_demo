package com.radiusinteractive.wing.fsm;


public interface FsmController {
    // Fsm states
    int STATE_0_SETUP = 0;
    int STATE_1_IDLE = 1;
    int STATE_2_BOOKED = 2;
    int STATE_3_RENTED = 3;

    //Fsm events
    String EVENT_USER_VERIFIED_AND_PAYMENT_VALIDATED =
            "event_user_verified_and_payment_validated";
    String EVENT_USER_UNVERIFIED_OR_PAYMENT_INVALIDATED =
            "event_user_unverified_or_payment_invalidated";
    String EVENT_SCOOTER_BOOKED = "event_scooter_booked";
    String EVENT_SCOOTER_UNBOOKED = "event_scooter_unbooked";
    String EVENT_SCOOTER_RENTED = "event_scooter_rented";
    String EVENT_SCOOTER_UNRENTED = "event_scooter_unrented";

    int getCurrentState();
    void transitionTo(int state);
    void fireEvent(String event);
}
