package com.radiusinteractive.wing.fsm;

import de.halfbit.tinymachine.StateHandler;
import de.halfbit.tinymachine.TinyMachine;

class FsmHandler {

    private static final String LOG_TAG = "fsm_handler";

    /**
     * Handlers for entry on any state
     */
    @StateHandler(state = StateHandler.STATE_ANY, type = StateHandler.Type.OnEntry)
    public void onEntryStateAny() {
        // This method is called when machine enters any new state
    }

    /**
     * Handlers for STATE_0_SETUP
     */
    @StateHandler(state = FsmController.STATE_0_SETUP, type = StateHandler.Type.OnEntry)
    public void onEntryState0Setup() {
        //TODO: add implementation for onEntry STATE_0_SETUP
    }

    @StateHandler(state = FsmController.STATE_0_SETUP, type = StateHandler.Type.OnExit)
    public void onExitState0Setup() {
        //TODO: add implementation for onExit STATE_0_SETUP
    }

    @StateHandler(state = FsmController.STATE_0_SETUP)
    public void onEventState0Setup(final String event, TinyMachine fsm) {
        switch(event){
            case FsmController.EVENT_USER_VERIFIED_AND_PAYMENT_VALIDATED:
                fsm.transitionTo(FsmController.STATE_1_IDLE);
                break;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Handlers for STATE_1_IDLE
     */
    @StateHandler(state = FsmController.STATE_1_IDLE, type = StateHandler.Type.OnEntry)
    public void onEntryState1Idle() {
        //TODO: add implementation for onEntry STATE_1_IDLE
    }

    @StateHandler(state = FsmController.STATE_1_IDLE, type = StateHandler.Type.OnExit)
    public void onExitState1Idle() {
        //TODO: add implementation for onExit STATE_1_IDLE
    }

    @StateHandler(state = FsmController.STATE_1_IDLE)
    public void onEventState1Idle (final String event, TinyMachine fsm) {
        switch(event){
            case FsmController.EVENT_USER_UNVERIFIED_OR_PAYMENT_INVALIDATED:
                fsm.transitionTo(FsmController.STATE_0_SETUP);
                break;
            case FsmController.EVENT_SCOOTER_BOOKED:
                fsm.transitionTo(FsmController.STATE_2_BOOKED);
                break;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Handlers for STATE_2_BOOKED
     */
    @StateHandler(state = FsmController.STATE_2_BOOKED, type = StateHandler.Type.OnEntry)
    public void onEntryState2Booked() {
        //TODO: add implementation for onEntry STATE_2_BOOKED
    }

    @StateHandler(state = FsmController.STATE_2_BOOKED, type = StateHandler.Type.OnExit)
    public void onExitSState2Booked() {
        //TODO: add implementation for onExit STATE_2_BOOKED
    }

    @StateHandler(state = FsmController.STATE_2_BOOKED)
    public void onEventState2Booked (final String event, TinyMachine fsm) {
        switch(event){
            case FsmController.EVENT_SCOOTER_RENTED:
                fsm.transitionTo(FsmController.STATE_3_RENTED);
                break;
            case FsmController.EVENT_SCOOTER_UNBOOKED:
                fsm.transitionTo(FsmController.STATE_1_IDLE);
                break;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Handlers for STATE_3_RENTED
     */
    @StateHandler(state = FsmController.STATE_3_RENTED, type = StateHandler.Type.OnEntry)
    public void onEntryState3Rented() {
        //TODO: add implementation for onEntry STATE_2_BOOKED
    }

    @StateHandler(state = FsmController.STATE_3_RENTED, type = StateHandler.Type.OnExit)
    public void onExitSState3Rented() {
        //TODO: add implementation for onExit STATE_2_BOOKED
    }

    @StateHandler(state = FsmController.STATE_3_RENTED)
    public void onEventState3Rented (final String event, TinyMachine fsm) {
        switch(event){
            case FsmController.EVENT_SCOOTER_UNRENTED:
                fsm.transitionTo(FsmController.STATE_1_IDLE);
                break;
            default:
                throw new IllegalStateException();
        }
    }

}
