package com.radiusinteractive.wing.fsm;

import de.halfbit.tinymachine.TinyMachine;

public class Fsm implements FsmController {

    private static final String LOG_TAG = "fsm";
    private TinyMachine mFsm;

    public Fsm(){
        mFsm = new TinyMachine(new FsmHandler(),STATE_0_SETUP);
    }

    public Fsm(final int state){
        mFsm = new TinyMachine(new FsmHandler(),state);
    }

    @Override
    public int getCurrentState() {
        return mFsm.getCurrentState();
    }

    @Override
    public void transitionTo(int state) {
        mFsm.transitionTo(state);
    }

    @Override
    public void fireEvent(String event) throws IllegalStateException {
        try {
            mFsm.fireEvent(event);
        }catch(IllegalStateException e){
            throw new IllegalStateException(String.format(illegalEventReceievedException,
                    getStateName(getCurrentState()),event));
        }
    }

    public static String getStateName(final int state){
        switch(state){
            case FsmController.STATE_0_SETUP:
                return "0_setup";
            case FsmController.STATE_1_IDLE:
                return "1_idle";
            case FsmController.STATE_2_BOOKED:
                return "2_booked";
            case FsmController.STATE_3_RENTED:
                return "3_rented";
            default:
                return null;
        }
    }

    private final String illegalEventReceievedException = "The current state: %s does not accept the fired event: %s.";

}
