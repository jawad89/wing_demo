package com.radiusinteractive.wing.user;


import android.arch.lifecycle.LiveData;

import com.radiusinteractive.wing.webService.ApiResponse;

public interface UserAuthService {
    LiveData<ApiResponse<User>> signIn (UserAuth userAuth);
    LiveData<ApiResponse<User>> register (UserAuth userAuth);
    LiveData<ApiResponse<String>> resetPassword (UserAuth userAuth);
    boolean isSignedIn ();
    void signOut();
}
