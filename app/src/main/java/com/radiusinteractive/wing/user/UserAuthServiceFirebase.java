package com.radiusinteractive.wing.user;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.radiusinteractive.wing.common.Logger;
import com.radiusinteractive.wing.webService.ApiResponse;

public class UserAuthServiceFirebase implements UserAuthService{
    private FirebaseAuth mAuth;
    private static final String LOG_TAG = "auth_serv";

    public UserAuthServiceFirebase(){
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public LiveData<ApiResponse<User>> signIn(final UserAuth userAuth) {
        final MutableLiveData<ApiResponse<User>> liveData = new MutableLiveData<>();
        final User user = new User(userAuth.getEmail());
        mAuth.signInWithEmailAndPassword(userAuth.getEmail(),userAuth.getPassword())
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        if(isEmailVerified()) {
                            liveData.setValue(new ApiResponse<>(user));
                        }else{
                            signOut();
                            liveData.setValue(new ApiResponse<User>(new Throwable(
                                    "Email is not yet verified, the user has been signed out." +
                                            " Please check your inbox for verification email."
                            )));
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Logger.d(LOG_TAG,"User sign in failed ");
                        liveData.setValue(new ApiResponse<User>(e));
                    }
                });

        return liveData;
    }

    @Override
    public LiveData<ApiResponse<User>> register(final UserAuth userAuth) {
        final MutableLiveData<ApiResponse<User>> liveData = new MutableLiveData<>();

        mAuth.createUserWithEmailAndPassword(userAuth.getEmail(),userAuth.getPassword())
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Logger.d(LOG_TAG,"User created, sending verification email");
                        sendVerificationEmail(liveData);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Logger.d(LOG_TAG,"User creation failed");
                        liveData.setValue(new ApiResponse<User>(
                                new Throwable("Unable to complete operation")));
                    }
                });

        return liveData;
    }

    private void sendVerificationEmail(final MutableLiveData<ApiResponse<User>> liveData){
        final FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser==null) {
            liveData.setValue(new ApiResponse<User>(
                    new Throwable("Unable to complete operation")));
            return;
        }

        Logger.d(LOG_TAG,"Sending verification email");
        currentUser.sendEmailVerification()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Logger.d(LOG_TAG,"Verification email sent successfully");
                        signOut();
                        liveData.setValue(new ApiResponse<>(new User(currentUser.getEmail())));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Logger.d(LOG_TAG,"Verification email failed");
                        signOut();
                        liveData.setValue(new ApiResponse<User>(e));
                    }
                });

    }

    @Override
    public LiveData<ApiResponse<String>> resetPassword(UserAuth userAuth) {
        final MutableLiveData<ApiResponse<String>> liveData = new MutableLiveData<>();

        mAuth.sendPasswordResetEmail(userAuth.getEmail())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        liveData.setValue(new ApiResponse<>("Customer password has been successfully reset"));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Logger.d(LOG_TAG,"Password reset failed");
                        liveData.setValue(new ApiResponse<String>(e));
                    }
                });

        return liveData;
    }

    @Override
    public boolean isSignedIn() {
        try {
            Logger.d(LOG_TAG, "Current user: " + mAuth.getCurrentUser().getEmail());
        }catch(NullPointerException e){
            Logger.d(LOG_TAG, "Current user is null:");
        }
        return mAuth.getCurrentUser()!=null;
    }

    @Override
    public void signOut() {
        Logger.d(LOG_TAG,"Signing out user");
        mAuth.signOut();
    }

    private boolean isEmailVerified() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        return currentUser!=null && currentUser.isEmailVerified();
    }
}
