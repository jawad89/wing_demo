package com.radiusinteractive.wing.user;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.radiusinteractive.wing.common.Logger;
import com.radiusinteractive.wing.webService.ApiResponse;

import java.util.concurrent.Executor;

import javax.inject.Inject;

public class UserRepository {

    private static final String LOG_TAG = "user_repo";
    private static final String PREFS_EMAIL = "user_email";

    private UserAuthService mAuth;
    private final UserDao mUserDao;
    private Executor mExecutor;
    private SharedPreferences mSharedPrefs;
    private User mUser;

    @Inject
    public UserRepository(UserDao userDao, UserAuthService userAuthService,
                          Executor executor, SharedPreferences sharedPrefs) {
        this.mAuth = userAuthService;
        this.mUserDao = userDao;
        mExecutor = executor;
        mSharedPrefs= sharedPrefs;
        initUser();
    }

    private void initUser(){
        if(mUser!=null)
            return;

        mUser = new User(mSharedPrefs.getString(PREFS_EMAIL,User.DEFAULT_EMAIL));
    }

    public User getCurrentUser(){
        return mUser;
    }

    public LiveData<ApiResponse<User>> signInUser(final UserAuth userAuth){
        final MediatorLiveData<ApiResponse<User>> response  = new MediatorLiveData<>();
        response.addSource(
                mAuth.signIn(userAuth), new Observer<ApiResponse<User>>() {
                    @Override
                    public void onChanged(@Nullable ApiResponse userApiResponse) {
                        response.setValue(userApiResponse);
                        if(userApiResponse.getError()==null){
                            mUser.setEmail(userAuth.getEmail());
                            saveUser();
                        }
                    }
                });

        return response;
    }

    public LiveData<ApiResponse<User>> createUser(final UserAuth userAuth){
        final MediatorLiveData<ApiResponse<User>> response  = new MediatorLiveData<>();

        response.addSource(
                mAuth.register(userAuth), new Observer<ApiResponse<User>>() {
                    @Override
                    public void onChanged(@Nullable ApiResponse userApiResponse) {
                        response.setValue(userApiResponse);
                        if(userApiResponse.getError()==null){
                            mUser.setEmail(userAuth.getEmail());
                            saveUser();
                        }
                    }
                });

        return response;
    }

    public LiveData<ApiResponse<String>> resetPassword(final UserAuth userAuth){
        return mAuth.resetPassword(userAuth);
    }

    public boolean isUserSignedIn(){
        return mAuth.isSignedIn();
    }

    private void saveUser(){
        Logger.d(LOG_TAG,"saving user to shared prefs");
        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putString(PREFS_EMAIL,mUser.getEmail());
        editor.apply();
    }



}
