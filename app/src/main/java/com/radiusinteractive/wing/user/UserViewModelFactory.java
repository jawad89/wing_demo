package com.radiusinteractive.wing.user;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.radiusinteractive.wing.app.WingApplication;

public class UserViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private WingApplication application;

    public UserViewModelFactory(WingApplication application) {
        this.application = application;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        T t = super.create(modelClass);
        if (t instanceof UserComponent.Injectable) {
            ((UserComponent.Injectable) t).inject(application.getUserComponent());
        }
        return t;
    }
}