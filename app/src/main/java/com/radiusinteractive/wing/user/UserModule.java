package com.radiusinteractive.wing.user;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    private final String SHARED_PREFS_NAME = "wing_prefs";
    private final String USER_DATABASE_NAME = "user-database";

    @Provides
    @Singleton
    UserRepository provideUserRepository(
            UserDao userDao, UserAuthService userAuthService,
            SharedPreferences sharedPrefs, Executor executor){
        return new UserRepository(userDao, userAuthService,executor,sharedPrefs);
    }

    @Provides
    @Singleton
    @Inject
    SharedPreferences provideSharedPreferences(Context context){
        return context.getSharedPreferences(SHARED_PREFS_NAME,Context.MODE_PRIVATE);
    }

    @Provides
    UserDao provideUserDao(UserDatabase userDatabase){
        return userDatabase.userDao();
    }

    @Provides
    @Singleton
    @Inject
    UserDatabase provideUserDatabase(Context context){
        return Room.databaseBuilder(context,
                UserDatabase.class, USER_DATABASE_NAME).build();
    }

    @Provides
    UserAuthService provideAuthenticator() {
        return new UserAuthServiceFirebase();
    }

    @Provides
    Executor provideExecutor(){
        return Executors.newSingleThreadExecutor();
    }

}
