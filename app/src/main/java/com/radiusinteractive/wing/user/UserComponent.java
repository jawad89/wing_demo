package com.radiusinteractive.wing.user;


import android.content.Context;

import com.radiusinteractive.wing.app.AppModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {UserModule.class,AppModule.class})
public interface UserComponent {

    Context getContext();

    void inject (UserProfileViewModel target);
    interface Injectable {
        void inject(UserComponent userComponent);
    }
}
