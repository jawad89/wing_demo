package com.radiusinteractive.wing.user;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class User{

    @Ignore
    public static final String DEFAULT_EMAIL = "";

    @PrimaryKey
    private String email;

    private String title;

    @ColumnInfo(name="first_name")
    private String firstName;

    @ColumnInfo(name="last_name")
    private String lastName;

    @ColumnInfo(name="driving_license_url")
    private String drivingLicenseUrl;

    @ColumnInfo(name="passport_url")
    private String passportUrl;

    @ColumnInfo(name="dob")
    private long dateOfBirth;

    @ColumnInfo(name="user_verified")
    private boolean isUserVerified;

    @Ignore
    public User(){ }

    public User(String email){
        this.email=email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDrivingLicenseUrl() {
        return drivingLicenseUrl;
    }

    public void setDrivingLicenseUrl(String drivingLicenseUrl) {
        this.drivingLicenseUrl = drivingLicenseUrl;
    }

    public String getPassportUrl() {
        return passportUrl;
    }

    public void setPassportUrl(String passportUrl) {
        this.passportUrl = passportUrl;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isUserVerified() {
        return isUserVerified;
    }

    public void setUserVerified(boolean userVerified) {
        isUserVerified = userVerified;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", title='" + title + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", drivingLicenseUrl='" + drivingLicenseUrl + '\'' +
                ", passportUrl='" + passportUrl + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", isUserVerified=" + isUserVerified +
                '}';
    }
}
