package com.radiusinteractive.wing.user;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.radiusinteractive.wing.webService.ApiResponse;

import javax.inject.Inject;


public class UserProfileViewModel extends ViewModel implements UserComponent.Injectable {
    private MutableLiveData<User> mUser;
    private MediatorLiveData<ApiResponse<User>> mLoginApiResponse;

    @Inject
    UserRepository mUserRepo;

    @Override
    public void inject(UserComponent userComponent) {
        userComponent.inject(this);
        init();
    }

    private void init() {
        if (this.mUser != null) {
            return;
        }
        mLoginApiResponse = new MediatorLiveData<>();
        mUser = new MutableLiveData<>();
        mUser.setValue(mUserRepo.getCurrentUser());
    }

    public LiveData<User> getUser() {
        return this.mUser;
    }

    public LiveData<ApiResponse<User>> signInUser(final UserAuth userAuth){
        return mUserRepo.signInUser(userAuth);
    }

    public LiveData<ApiResponse<User>> createUser(final UserAuth userAuth){
        return mUserRepo.createUser(userAuth);
    }

    public LiveData<ApiResponse<String>> resetPassword(final UserAuth userAuth){
        return mUserRepo.resetPassword(userAuth);
    }

    public boolean isUserSignedIn(){
        return mUserRepo.isUserSignedIn();
    }
}
