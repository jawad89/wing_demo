package com.radiusinteractive.wing.authentication;

import android.view.View;

public interface AuthenticatorContract {

    interface AuthView {
        int VIEW_STATE_SPLASH = 0;
        int VIEW_STATE_SIGNIN = 1;
        int VIEW_STATE_REGISTER = 2;
        int VIEW_STATE_FORGOT_PASSWORD = 3;

        void setViewSplash();
        void setViewRegister();
        void setViewLogin();
        void setViewForgotPassword();
        void showProgressDialog();
        void hideProgressDialog();
        View getRootView();
        int getCurrentView();
        void clearPassword();
    }

    interface AuthPresenter {
        void loginUser(String email, String password);
        void createUserAccount(String email, String password);
        void resetUserPassword(String email);
        void useWingAsGuest();
    }
}
