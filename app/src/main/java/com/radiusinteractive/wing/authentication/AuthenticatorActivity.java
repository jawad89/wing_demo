package com.radiusinteractive.wing.authentication;

import android.os.Bundle;

import com.radiusinteractive.wing.R;
import com.radiusinteractive.wing.common.BaseActivity;
import com.radiusinteractive.wing.util.ActivityUtils;

public class AuthenticatorActivity extends BaseActivity {

    static final String LOG_TAG = "auth";
    private AuthenticatorViewFragment mAuthView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authenticator_activity);

        mAuthView =
                (AuthenticatorViewFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (mAuthView == null) {
            // Create the fragment
            mAuthView = AuthenticatorViewFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mAuthView, R.id.contentFrame);
        }


    }

}
