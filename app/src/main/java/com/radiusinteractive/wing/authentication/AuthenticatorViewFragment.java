package com.radiusinteractive.wing.authentication;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.radiusinteractive.inputlayoutview.InputLayoutView;
import com.radiusinteractive.wing.R;
import com.radiusinteractive.wing.app.WingApplication;
import com.radiusinteractive.wing.common.Logger;
import com.radiusinteractive.wing.common.WingProgressDialog;
import com.radiusinteractive.wing.user.User;
import com.radiusinteractive.wing.user.UserAuth;
import com.radiusinteractive.wing.user.UserProfileViewModel;
import com.radiusinteractive.wing.user.UserViewModelFactory;
import com.radiusinteractive.wing.util.Utils;
import com.radiusinteractive.wing.webService.ApiResponse;


import java.util.Locale;

public class AuthenticatorViewFragment extends LifecycleFragment
        implements AuthenticatorContract.AuthView {

    private ImageView logoIV;
    private TextView titleTV;
    private TextView forgotPasswordTV;
    private TextView accountExistsTV;
    private InputLayoutView emailILV;
    private InputLayoutView passwordILV;
    private InputLayoutView confirmPasswordILV;
    private Button actionButton;
    private Button guestButton;
    private ProgressBar progressBar;

    private final int animationDuration = 350;
    private final int PASSWORD_MIN_LENGTH = 8;
    private int currentView;

    private static final String LOG_TAG = "auth_view";

    UserProfileViewModel mUserProfileViewModel;

    public AuthenticatorViewFragment() {
    }

    public static AuthenticatorViewFragment newInstance() {
        return new AuthenticatorViewFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mUserProfileViewModel = ViewModelProviders
                .of(this, new UserViewModelFactory(
                        (WingApplication)getActivity().getApplication()))
                .get(UserProfileViewModel.class);

        initView(mUserProfileViewModel.getUser().getValue());
        setObservers();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.authenticator_view_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
    }

    void bindViews(View rootView){
        logoIV = (ImageView) rootView.findViewById(R.id.logoIV);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        titleTV = (TextView) rootView.findViewById(R.id.titleTV);
        forgotPasswordTV = (TextView) rootView.findViewById(R.id.forgotPasswordTV);
        accountExistsTV = (TextView) rootView.findViewById(R.id.accountExistsTV);
        emailILV = (InputLayoutView) rootView.findViewById(R.id.emailILV);
        passwordILV = (InputLayoutView) rootView.findViewById(R.id.passwordILV);
        confirmPasswordILV = (InputLayoutView) rootView.findViewById(R.id.confirmPasswordILV);
        actionButton = (Button) rootView.findViewById(R.id.actionButton);
        guestButton = (Button) rootView.findViewById(R.id.guestButton);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        actionButton.setOnClickListener(OnClickActionButton);
        guestButton.setOnClickListener(OnClickGuestButton);
        forgotPasswordTV.setOnClickListener(OnForgotPassword);
        accountExistsTV.setOnClickListener(OnClickAccountExists);

        emailILV.setTextInputFinishedListener(emailInputFinishedListener);
        passwordILV.setTextInputFinishedListener(passwordInputFinishedListener);
        confirmPasswordILV.setTextInputFinishedListener(confirmPasswordInputFinishedListener);

    }

    private void initView(User user){
        Logger.d(LOG_TAG,"Current user: " + user);
        if(mUserProfileViewModel.isUserSignedIn()) {
            setViewSplash();
            //TODO: make an API request to server to get user state
            return;
        }

        //check if user is registered
        if(!user.getEmail().isEmpty())
            setViewLogin();
        else
            setViewRegister();
    }

    private void setObservers(){

    }

    @Override
    public void setViewSplash(){
        progressBar.setVisibility(View.VISIBLE);
        translateViewToScreenCenter(logoIV);

        titleTV.setVisibility(View.GONE);
        emailILV.setVisibility(View.GONE);
        passwordILV.setVisibility(View.GONE);
        confirmPasswordILV.setVisibility(View.GONE);
        forgotPasswordTV.setVisibility(View.GONE);
        accountExistsTV.setVisibility(View.GONE);
        guestButton.setVisibility(View.GONE);
        actionButton.setVisibility(View.GONE);

        currentView=VIEW_STATE_SPLASH;

    }

    @Override
    public void setViewLogin(){
        if(logoIV.getTranslationY()==0)
            translateViewToScreenTop(logoIV);

        titleTV.setVisibility(View.VISIBLE);
        emailILV.setVisibility(View.VISIBLE);
        passwordILV.setVisibility(View.VISIBLE);
        forgotPasswordTV.setVisibility(View.VISIBLE);
        accountExistsTV.setVisibility(View.VISIBLE);
        guestButton.setVisibility(View.VISIBLE);
        actionButton.setVisibility(View.VISIBLE);

        progressBar.setVisibility(View.GONE);
        confirmPasswordILV.setVisibility(View.GONE);

        titleTV.setText(getString(R.string.login));
        accountExistsTV.setText(getString(R.string.dont_have_account_register_now));
        actionButton.setText(getString(R.string.login));

        currentView= VIEW_STATE_SIGNIN;
    }

    @Override
    public void setViewRegister(){
        if(logoIV.getTranslationY()==0)
            translateViewToScreenTop(logoIV);

        titleTV.setVisibility(View.VISIBLE);
        emailILV.setVisibility(View.VISIBLE);
        passwordILV.setVisibility(View.VISIBLE);
        confirmPasswordILV.setVisibility(View.VISIBLE);
        accountExistsTV.setVisibility(View.VISIBLE);
        guestButton.setVisibility(View.VISIBLE);
        actionButton.setVisibility(View.VISIBLE);

        progressBar.setVisibility(View.GONE);
        forgotPasswordTV.setVisibility(View.GONE);

        titleTV.setText(getString(R.string.register));
        accountExistsTV.setText(getString(R.string.already_have_an_account_login_here));
        actionButton.setText(getString(R.string.register));

        currentView=VIEW_STATE_REGISTER;
    }

    @Override
    public void setViewForgotPassword(){
        if(logoIV.getTranslationY()==0)
            translateViewToScreenTop(logoIV);

        titleTV.setVisibility(View.VISIBLE);
        emailILV.setVisibility(View.VISIBLE);
        accountExistsTV.setVisibility(View.VISIBLE);
        guestButton.setVisibility(View.VISIBLE);
        actionButton.setVisibility(View.VISIBLE);

        progressBar.setVisibility(View.GONE);
        passwordILV.setVisibility(View.GONE);
        confirmPasswordILV.setVisibility(View.GONE);
        forgotPasswordTV.setVisibility(View.GONE);

        titleTV.setText(getString(R.string.reset_password));
        accountExistsTV.setText(getString(R.string.remember_password_login_here));
        actionButton.setText(getString(R.string.reset_password_caps));

        currentView=VIEW_STATE_FORGOT_PASSWORD;
    }

    @Override
    public View getRootView() {
        return getView();
    }

    @Override
    public void showProgressDialog() {
        WingProgressDialog.show(getActivity());
    }

    @Override
    public void hideProgressDialog() {
        WingProgressDialog.hide();
    }

    @Override
    public int getCurrentView() {
        return currentView;
    }

    @Override
    public void clearPassword() {
        passwordILV.setText("");
        confirmPasswordILV.setText("");
    }

    private boolean isInputValid() {
        switch (currentView){
            case VIEW_STATE_SPLASH:
                return true;
            case VIEW_STATE_REGISTER:
                return !(emailILV.isError() || passwordILV.isError()
                        || confirmPasswordILV.isError());
            case VIEW_STATE_SIGNIN:
                return !(emailILV.isError() || passwordILV.isError());
            case VIEW_STATE_FORGOT_PASSWORD:
                return !emailILV.isError();
        }

        return false;
    }

    private View.OnClickListener OnClickActionButton = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Logger.d(LOG_TAG,"OnClickActionButton");

            if(isEmailInputValid(emailILV.getText())!=null || !isInputValid()){
                Utils.showSnackbarMessage("Please provide all the required fields",getView());
                return;
            }

            switch(currentView){
                case VIEW_STATE_REGISTER:
                    createUser();
                    break;
                case VIEW_STATE_SIGNIN:
                    signInUser();
                    break;
                case VIEW_STATE_FORGOT_PASSWORD:
                    resetPassword();
                    break;
            }
        }

    };

    private void signInUser(){
        showProgressDialog();
        final UserAuth userAuth = new UserAuth(emailILV.getText(),passwordILV.getText());
        final LiveData<ApiResponse<User>> observable = mUserProfileViewModel.signInUser(userAuth);
        observable.observe(this, new Observer<ApiResponse<User>>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                observable.removeObservers(AuthenticatorViewFragment.this);
                hideProgressDialog();
                if(apiResponse==null)
                    Utils.showSnackbarMessage("Failed to complete operation",getRootView());
                else if(apiResponse.getError()==null)
                    Utils.showSnackbarMessage("User successfully logged in",getRootView());
                else
                    Utils.showSnackbarMessage(apiResponse.getError().getMessage(),getRootView());

            }
        });
    }

    private void createUser(){
        showProgressDialog();
        final UserAuth userAuth = new UserAuth(emailILV.getText(),passwordILV.getText());
        final LiveData<ApiResponse<User>> observable = mUserProfileViewModel.createUser(userAuth);
        observable.observe(this, new Observer<ApiResponse<User>>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                observable.removeObservers(AuthenticatorViewFragment.this);
                hideProgressDialog();
                if(apiResponse==null)
                    Utils.showSnackbarMessage("Failed to complete operation",getRootView());
                else if(apiResponse.getError()==null) {
                    Utils.showSnackbarMessage("Verification email has been sent, please check your email inbox for the verification link", getRootView());
                    setViewLogin();
                }else
                    Utils.showSnackbarMessage(apiResponse.getError().getMessage(),getRootView());

            }
        });
    }

    private void resetPassword(){
        showProgressDialog();
        final UserAuth userAuth = new UserAuth(emailILV.getText(),passwordILV.getText());
        final LiveData<ApiResponse<String>> observable = mUserProfileViewModel.resetPassword(userAuth);
        observable.observe(this, new Observer<ApiResponse<String>>() {
            @Override
            public void onChanged(@Nullable ApiResponse<String> apiResponse) {
                observable.removeObservers(AuthenticatorViewFragment.this);
                hideProgressDialog();
                if(apiResponse==null)
                    Utils.showSnackbarMessage("Failed to complete operation",getRootView());
                else if(apiResponse.getError()==null)
                    Utils.showSnackbarMessage(apiResponse.getResponse(),getRootView());
                else
                    Utils.showSnackbarMessage(apiResponse.getError().getMessage(),getRootView());

            }
        });
    }

    private View.OnClickListener OnClickGuestButton = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Logger.d(LOG_TAG,"OnClickGuestButton");
            //mAuthPresenter.useWingAsGuest();
        }

    };

    private View.OnClickListener OnClickAccountExists = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Logger.d(LOG_TAG,"OnClickAccountExists");

            switch(currentView){
                case VIEW_STATE_SIGNIN:
                    setViewRegister();
                    break;
                case VIEW_STATE_REGISTER:
                    setViewLogin();
                    break;
                case VIEW_STATE_FORGOT_PASSWORD:
                    setViewLogin();
                    break;
            }
        }

    };

    private View.OnClickListener OnForgotPassword = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Logger.d(LOG_TAG,"OnForgotPassword");
            setViewForgotPassword();
        }

    };

    private InputLayoutView.OnTextInputFinishedListener emailInputFinishedListener = new InputLayoutView.OnTextInputFinishedListener() {
        @Override
        public void onTextInputFinished() {
            emailILV.setError(isEmailInputValid(emailILV.getText()));
        }
    };

    private InputLayoutView.OnTextInputFinishedListener passwordInputFinishedListener = new InputLayoutView.OnTextInputFinishedListener() {
        @Override
        public void onTextInputFinished() {
            passwordILV.setError(isPasswordInputValid(passwordILV.getText()));
        }
    };

    private InputLayoutView.OnTextInputFinishedListener confirmPasswordInputFinishedListener = new InputLayoutView.OnTextInputFinishedListener() {
        @Override
        public void onTextInputFinished() {
            confirmPasswordILV.setError(isConfirmPasswordInputValid(
                    confirmPasswordILV.getText(), passwordILV.getText()));
        }
    };

    private void translateViewToScreenTop(View view){
        view.animate()
                .translationY(Utils.dp2pxls(getActivity(),-210))
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(animationDuration);
    }

    private void translateViewToScreenCenter(View view){
        view.animate()
                .translationY(Utils.dp2pxls(getActivity(),0))
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(animationDuration);
    }

    @Nullable
    private String isEmailInputValid(String input){
        if (input.isEmpty()) {
            return getString(R.string.email_empty);
        } else if (!isEmailFormatValid(input)){
            return getString(R.string.email_invalid);
        }
        return null;
    }

    private boolean isEmailFormatValid(final String email) {
        return !TextUtils.isEmpty(email) &&
                android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Nullable
    private String isPasswordInputValid(String input){
        if (input.isEmpty()) {
            return getString(R.string.password_empty);
        } else if (input.length()<PASSWORD_MIN_LENGTH){
            return String.format(Locale.ENGLISH,
                    getString(R.string.password_length_error),
                    PASSWORD_MIN_LENGTH);
        }
        return null;
    }

    @Nullable
    private String isConfirmPasswordInputValid(String thisInput, String otherInput){
        String passwordInputValid = isPasswordInputValid(thisInput);
        if(passwordInputValid!=null)
            return passwordInputValid;

        return thisInput.equals(otherInput) ?
                null:getString(R.string.passwords_not_matching);
    }


}
