package com.radiusinteractive.wing.app;

import android.app.Application;

import com.radiusinteractive.wing.user.DaggerUserComponent;
import com.radiusinteractive.wing.user.UserComponent;


public class WingApplication extends Application {

    private UserComponent mUserComponent;

    public UserComponent getUserComponent() {
        return mUserComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mUserComponent = initDagger(this);
    }

    protected UserComponent initDagger(WingApplication application) {
        return DaggerUserComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }
}
