package com.radiusinteractive.wing.common;

import android.util.Log;


public class Logger {
    public static final boolean DEBUG = true;
    private static final String LOG_TAG = "wing_";

    public static void e(final String tag, String message){
        if(!DEBUG)
            return;

        Log.e(LOG_TAG +tag,message);
    }

    public static void d(final String tag, String message){
        if(!DEBUG)
            return;

        Log.d(LOG_TAG +tag,message);
    }

    public static void w(final String tag, String message){
        if(!DEBUG)
            return;

        Log.w(LOG_TAG +tag,message);
    }

    public static void i(final String tag, String message){
        if(!DEBUG)
            return;

        Log.i(LOG_TAG +tag,message);
    }

    public static void v(final String tag, String message){
        if(!DEBUG)
            return;

        Log.v(LOG_TAG +tag,message);
    }
}
