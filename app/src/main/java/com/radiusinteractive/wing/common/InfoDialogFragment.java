package com.radiusinteractive.wing.common;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.radiusinteractive.wing.R;


public class InfoDialogFragment extends DialogFragment {

    private static final String TAG = "error";
    private iDialogListener dialogListener;

    private TextView messageTV,titleTV;
    private ImageView iconIV;
    private Button positiveButton, negativeButton;
    private int dialogType,dialogCode;
    private String title, message,positiveText,negativeText;

    public static final int TYPE_CONFIRMATION = 0;
    public static final int TYPE_INFO = 1;
    public static final int TYPE_ERROR = 2;
    public static final int TYPE_FATAL_ERROR = 3;

    private static final String DIALOG_CODE = "dialog_code";
    private static final String DIALOG_TYPE = "dialog_type";
    private static final String MESSAGE = "message";
    private static final String NEGATIVE_TEXT = "negative_text";
    private static final String POSITIVE_TEXT = "positive_text";
    private static final String TITLE = "title";

    public InfoDialogFragment() {
        // Required empty public constructor
    }

    public static void launch(AppCompatActivity context, InfoDialogFragment infoDialogFragment){
        infoDialogFragment.show(context.getSupportFragmentManager(), "info_dialog");
    }

    private static InfoDialogFragment newInstance(DialogBuilder builder){
        InfoDialogFragment f = new InfoDialogFragment();
        Bundle args = new Bundle();
        args.putInt(DIALOG_TYPE,builder.dialogType);
        args.putInt(DIALOG_CODE,builder.dialogCode);
        args.putString(TITLE,builder.title);
        args.putString(MESSAGE,builder.message);
        args.putString(NEGATIVE_TEXT,builder.negativeText);
        args.putString(POSITIVE_TEXT,builder.positiveText);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,
                R.style.WingDialogTheme);
        Bundle bundle = getArguments();
        title = bundle.getString(TITLE);
        message = bundle.getString(MESSAGE);
        negativeText = bundle.getString(NEGATIVE_TEXT);
        positiveText = bundle.getString(POSITIVE_TEXT);
        dialogType=bundle.getInt(DIALOG_TYPE);
        dialogCode=bundle.getInt(DIALOG_CODE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.info_dialog_fragment, container,false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        messageTV = (TextView) view.findViewById(R.id.messageTV);
        titleTV = (TextView) view.findViewById(R.id.titleTV);
        iconIV = (ImageView) view.findViewById(R.id.iconIV);
        negativeButton = (Button) view.findViewById(R.id.negativeButton);
        positiveButton = (Button) view.findViewById(R.id.postiveButton);

        setDialogUi();
    }

    private void setDialogUi(){
        titleTV.setText(title);
        if(message==null)
            messageTV.setVisibility(View.GONE);
        else
            messageTV.setText(message);

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.onPositiveButtonClick(dialogCode);
                dismiss();
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.onNegativeButtonClick(dialogCode);
                dismiss();
            }
        });

        positiveButton.setText(positiveText);
        if(negativeText!=null)
            negativeButton.setText(negativeText);

        switch(dialogType){
            case TYPE_INFO:
                iconIV.setImageResource(R.drawable.ic_info_white_48dp);
                negativeButton.setVisibility(View.GONE);
                break;
            case TYPE_CONFIRMATION:
                iconIV.setImageResource(R.drawable.ic_done_with_white_circle_bg);
                negativeButton.setVisibility(View.GONE);
                break;
            case TYPE_ERROR:
                iconIV.setImageResource(R.drawable.ic_warning_white_48dp);
                negativeButton.setVisibility(View.VISIBLE);
                break;
            case TYPE_FATAL_ERROR:
                iconIV.setImageResource(R.drawable.ic_warning_white_48dp);
                negativeButton.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG,"onAttach(context): Fragment attached");
        initializeListener(context);
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        Log.d(TAG,"onAttach(activity): Fragment attached");
        initializeListener(context);
    }

    private void initializeListener(Context context) {
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the iReviewCommunicator so we can send events to the host
            dialogListener = (iDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement iDialogListener");
        }
    }

    public interface iDialogListener {
        void onPositiveButtonClick(int dialogCode);
        void onNegativeButtonClick(int dialogCode);
    }

    public static class DialogBuilder{
        private int dialogType,dialogCode;
        private String title, message,positiveText,negativeText;

        public DialogBuilder(int dialogCode,int dialogType,String title){
            this.dialogCode=dialogCode;
            this.dialogType=dialogType;
            this.title=title;
        }

        public DialogBuilder message(String message){
            this.message=message;
            return this;
        }

        public DialogBuilder positiveText(String positiveText){
            this.positiveText=positiveText;
            return this;
        }

        public DialogBuilder negativeText(String negativeText){
            this.negativeText=negativeText;
            return this;
        }

        public InfoDialogFragment build(){
            return newInstance(this);
        }
    }
}
