package com.radiusinteractive.wing.common;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.radiusinteractive.wing.R;

public class WingProgressDialog extends DialogFragment {

    private static WingProgressDialog mDialog;
    public WingProgressDialog(){
        // Required empty public constructor
    }

    public static WingProgressDialog show(Activity context) {
        if(mDialog==null) {
            mDialog = new WingProgressDialog();
        }

        mDialog.show(context.getFragmentManager(), "progress_dialog");
        return mDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations =  R.style.WingDialogTheme;
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,
                R.style.WingDialogTheme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.progress_fullscreen, container,false);
    }

    public static void hide(){
        if(mDialog==null)
            return;

        mDialog.dismiss();
    }
}
