package com.radiusinteractive.inputlayoutview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

/**
 * This EditText class over-rides the onKeyPreIme method to get the back key press event
 * when the EditText is in focus
 */

public class CustomEditText extends android.support.v7.widget.AppCompatEditText {

    private ImeBackListener mOnImeBack;

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK &&
                event.getAction() == KeyEvent.ACTION_UP) {
            if (mOnImeBack != null)
                mOnImeBack.onImeBackPress();
        }
        return super.dispatchKeyEvent(event);
    }

    public void setOnEditTextImeBackListener(ImeBackListener listener) {
        mOnImeBack = listener;
    }

    public interface ImeBackListener {
        void onImeBackPress();
    }
}
