package com.radiusinteractive.inputlayoutview;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;


public class InputLayoutView extends LinearLayout {

    public static final String LOG_TAG = "ilv";

    public static final int INPUT_TYPE_TEXT = 0;
    public static final int INPUT_TYPE_BUTTON = 1;
    public static final int INPUT_TYPE_PHOTO = 2;

    private TextView textViewTV,titleTV,errorTV;
    private CustomEditText editTextET;
    private ImageView arrowIV;
    private RoundedImageView photoRIV;
    private View inputGroup, addPhotoGroup;

    private boolean isError;
    private File imageFile;
    private int mInputType;
    private OnButtonClickListener mButtonClickListener;
    private OnTextInputFinishedListener mTextInputFinishedListener;

    public InputLayoutView(Context context) {
        super(context);
        init(context,null);
    }

    public InputLayoutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    private void init(Context context,AttributeSet attrs){
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.InputLayout, 0, 0);

        String mText,mHint,mErrorText,mTitle;
        boolean isSingleLine;
        int mTextInputType;

        try {
            mText = a.getString(R.styleable.InputLayout_text);
            mHint = a.getString(R.styleable.InputLayout_hint);
            mErrorText = a.getString(R.styleable.InputLayout_error_text);
            mTitle = a.getString(R.styleable.InputLayout_title);
            mInputType = a.getInteger(R.styleable.InputLayout_input_type,INPUT_TYPE_TEXT);
            isSingleLine = a.getBoolean(R.styleable.InputLayout_is_single_line,true);
            mTextInputType =  a.getInteger(R.styleable.InputLayout_text_input_type,0);
        } finally {
            a.recycle();
        }

        inflate(getContext(),R.layout.input_layout,this);

        textViewTV = (TextView) findViewById(R.id.textViewTV);
        titleTV = (TextView) findViewById(R.id.titleTV);
        errorTV = (TextView) findViewById(R.id.errorTV);
        editTextET = (CustomEditText) findViewById(R.id.editTextET);
        arrowIV = (ImageView) findViewById(R.id.arrowIV);
        photoRIV = (RoundedImageView) findViewById(R.id.photoRoundedIV);
        addPhotoGroup = findViewById(R.id.addPhotoGroup);
        inputGroup = findViewById(R.id.inputGroup);

        LayoutTransition layoutTransition = new LayoutTransition();
        setLayoutTransition(layoutTransition);
        setOrientation(LinearLayout.VERTICAL);
        setInputType(mInputType);
        setError(mErrorText);
        setTitle(mTitle);
        setHint(mHint);
        setText(mText);
        setSingleLine(isSingleLine);
        setTextInputType(mTextInputType);

        textViewTV.setOnClickListener(textViewOnClickListener);
        photoRIV.setOnClickListener(photoOnClickListener);
        editTextET.setOnEditTextImeBackListener(editTextBackListener);
        editTextET.setOnFocusChangeListener(editTextFocusLostListener);
        editTextET.setOnEditorActionListener(editTextActionDoneListener);

    }

    public void setInputType(final int inputType){
        switch (inputType){
            case INPUT_TYPE_TEXT:
                editTextET.setVisibility(VISIBLE);
                textViewTV.setVisibility(GONE);
                arrowIV.setVisibility(GONE);
                addPhotoGroup.setVisibility(GONE);
                photoRIV.setVisibility(GONE);
                break;
            case INPUT_TYPE_BUTTON:
                editTextET.setVisibility(GONE);
                textViewTV.setVisibility(VISIBLE);
                arrowIV.setVisibility(VISIBLE);
                addPhotoGroup.setVisibility(GONE);
                photoRIV.setVisibility(GONE);
                break;
            case INPUT_TYPE_PHOTO:
                LayoutParams params = new LayoutParams(
                        LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

                inputGroup.setLayoutParams(params);
                editTextET.setVisibility(GONE);
                textViewTV.setVisibility(GONE);
                arrowIV.setVisibility(GONE);
                setPhotoVisibility();
                break;
        }
    }

    public int getInputType(){
        return mInputType;
    }

    public void setTitle(final String title){
        titleTV.setText(title);
    }

    public String getTitle(){
        return titleTV.getText().toString();
    }

    public void setHint(final String hint){
        editTextET.setHint(hint);
    }

    public String getHint(){
        return editTextET.getHint().toString();
    }

    public void setText(final String text){
        if(text==null)
            return;

        editTextET.setText(text);
        textViewTV.setText(text);
    }

    public String getText(){
        switch (mInputType){
            case INPUT_TYPE_TEXT:
                return editTextET.getText().toString();
            default:
                return textViewTV.getText().toString();
        }
    }

    public void setError(final String errorText){
        if(errorText==null)
            isError=false;
        else {
            isError = true;
            errorTV.setText(errorText);
        }

        if(isError) {
            errorTV.setVisibility(VISIBLE);
            inputGroup.setBackgroundResource(R.drawable.input_layout_error_bg);
        }else{
            errorTV.setVisibility(GONE);
            inputGroup.setBackgroundResource(R.drawable.input_layout_normal_bg);
        }

    }

    public void setTextInputType(final int textInputType){
        switch(textInputType){
            case 0:
                editTextET.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                break;
            case 1:
                editTextET.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
            case 2:
                editTextET.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
            case 3:
                editTextET.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS);
                break;
        }
    }

    public EditText getEditTextView(){
        return editTextET;
    }

    public String getErrorText(){
        return errorTV.getText().toString();
    }

    public boolean isError(){
        return isError;
    }

    public void setSingleLine(boolean isSingleLine){
        editTextET.setSingleLine(isSingleLine);
        textViewTV.setSingleLine(isSingleLine);
    }

    public void setPhotoVisibility(){
        if(mInputType != INPUT_TYPE_PHOTO)
            return;

        if(imageFile ==null) {
            addPhotoGroup.setVisibility(VISIBLE);
            photoRIV.setVisibility(VISIBLE);
        }else{
            addPhotoGroup.setVisibility(GONE);
            photoRIV.setVisibility(VISIBLE);
            photoRIV.setImageURI(Uri.fromFile(imageFile));
        }
    }

    public void setImageFile(File imageFile){
        this.imageFile =imageFile;
        setPhotoVisibility();
    }

    public void setImageBitmap(Bitmap imageBitmap){
        addPhotoGroup.setVisibility(GONE);
        photoRIV.setVisibility(VISIBLE);
        photoRIV.setImageBitmap(imageBitmap);
    }

    private boolean editTextETHasFocus;
    private View.OnFocusChangeListener editTextFocusLostListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(editTextETHasFocus && !hasFocus){
                Log.d(LOG_TAG,"EditText has lost focus");
                mTextInputFinishedListener.onTextInputFinished();
            }

            editTextETHasFocus = hasFocus;
        }
    };

    private EditText.OnEditorActionListener editTextActionDoneListener = new EditText.OnEditorActionListener(){
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                Log.d(LOG_TAG,"Done button pressed");
                mTextInputFinishedListener.onTextInputFinished();
            }
            return false;
        }
    };

    private CustomEditText.ImeBackListener editTextBackListener = new CustomEditText.ImeBackListener() {
        @Override
        public void onImeBackPress() {
            Log.d(LOG_TAG,"Back button pressed");
            mTextInputFinishedListener.onTextInputFinished();
        }
    };

    private View.OnClickListener textViewOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mInputType==INPUT_TYPE_BUTTON) {
                if (mButtonClickListener != null) {
                    mButtonClickListener.onButtonClick();
                }
            }
        }
    };

    private View.OnClickListener photoOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mInputType==INPUT_TYPE_PHOTO) {
                if (mButtonClickListener != null) {
                    mButtonClickListener.onButtonClick();
                }
            }
        }
    };

    public void setButtonClickListener(OnButtonClickListener onButtonClickListener){
        this.mButtonClickListener = onButtonClickListener;
    }

    public void setTextInputFinishedListener (OnTextInputFinishedListener onTextInputFinishedListener){
        this.mTextInputFinishedListener = onTextInputFinishedListener;
    }

    public interface OnButtonClickListener {
        void onButtonClick();
    }

    public interface OnTextInputFinishedListener{
        void onTextInputFinished();
    }

}
